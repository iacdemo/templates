package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

func main() {
	renderEnv()
	renderFromJson("common/config")
	renderFromJson("vdom/settings")
	renderFromJson("vdom/interfaces")
	renderFromJson("vdom/routing")
	renderFromJson("aci/aci")
	cmd := exec.Command("terraform", "fmt", "output")
	cmd.Run()
	fmt.Printf("Template generation completed.\n")
}

func renderFromJson(name string) {
	input, _ := ioutil.ReadFile("../configs/" + name + ".json")
	// input, _ := ioutil.ReadFile("./configs/" + name + ".json") // for dev
	if input == nil {
		return
	}
	funcMap := template.FuncMap{
		"formatValue":     formatValue,
		"flatten":         flatten,
		"siden":           siden,
		"d42VlanLookup":   d42VlanLookup,
		"d42SubnetLookup": d42SubnetLookup,
		"plusOne":         plusOne,
	}
	t := template.Must(template.New(name).Funcs(funcMap).ParseGlob("./gotemplates/*/*.gotmpl"))
	m := map[string]interface{}{}
	if err := json.Unmarshal([]byte(input), &m); err != nil {
		panic(err)
	}
	var buf bytes.Buffer
	if err := t.ExecuteTemplate(&buf, name, m); err != nil {
		panic(err)
	}
	perm := int(0755)

	// os.MkdirAll("./output/", os.FileMode(perm))
	// os.WriteFile("./output/"+siden(name)+".tf", buf.Bytes(), os.FileMode(perm)) // dev
	os.WriteFile("../"+siden(name)+".tf", buf.Bytes(), os.FileMode(perm))
}

func renderEnv() {
	input, _ := ioutil.ReadFile("../configs/common/config.json")
	// input, _ := ioutil.ReadFile("./configs/common/config.json") // for dev
	if input == nil {
		panic("config.json MUST be defined.")
	}
	funcMap := template.FuncMap{}
	t := template.Must(template.New("").Funcs(funcMap).ParseFiles("./gotemplates/common/env.gotmpl"))
	m := map[string]interface{}{}
	if err := json.Unmarshal([]byte(input), &m); err != nil {
		panic(err)
	}
	var buf bytes.Buffer
	if err := t.ExecuteTemplate(&buf, "common/env", m); err != nil {
		panic(err)
	}
	perm := int(0755)

	// os.MkdirAll("./output/", os.FileMode(perm))
	// os.WriteFile("./output/tf.env", buf.Bytes(), os.FileMode(perm)) // dev
	os.WriteFile("../tf.env", buf.Bytes(), os.FileMode(perm))
}

func formatValue(input interface{}) interface{} {
	switch t := input.(type) {
	case []interface{}:
		v, err := json.Marshal(t)
		if err != nil {
			return ""
		}
		eot := fmt.Sprintf(`<<-EOT
		%s
		EOT`, v)
		return eot
	}
	v, err := json.Marshal(input)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%s", v)
}

func d42VlanLookup(input string) map[string]interface{} {
	o := map[string]map[string]interface{}{
		"EXT-INTERSITE": {
			"tag":      "L2-DOMAIN-EXT-IACLAB",
			"vlan_min": 2300,
			"vlan_max": 2500,
		},
		"ACI-L2": {
			"tag":      "L2-DOMAIN-ACI-IACLAB-L2",
			"vlan_min": 250,
			"vlan_max": 499,
		},
		"ACI-L3": {
			"tag":      "L2-DOMAIN-ACI-IACLAB-L3",
			"vlan_min": 100,
			"vlan_max": 249,
		},
	}
	if o[input] == nil {
		panic("Dynamic VLAN lookup failed.")
	}
	return o[input]
}

func d42SubnetLookup(input string) map[string]interface{} {
	o := map[string]map[string]interface{}{
		"EXT-INTERSITE": {
			"parent_vlan_id": 1498,
			"tag":            "L2-DOMAIN-EXT-IACLAB",
		},
		"ACI-L2": {
			"tag": "L2-DOMAIN-ACI-IACLAB-L2",
		},
		"ACI-L3": {
			"parent_vlan_id": 1497,
			"tag":            "L2-DOMAIN-ACI-IACLAB-L3",
		},
		"EXT-SINGLE-IP": {
			"parent_vlan_id": 1496,
			"tag":            "L2-DOMAIN-EXT-IACLAB",
		},
	}
	if o[input] == nil {
		panic("Dynamic Subnet lookup failed.")
	}
	return o[input]
}

func flatten(s string) string {
	s = strings.ReplaceAll(s, "-", "_")
	s = strings.ReplaceAll(s, ".", "_")
	return s
}

func siden(s string) string {
	s = strings.ReplaceAll(s, "/", "_")
	s = strings.ReplaceAll(s, ".", "_")
	return s
}

func plusOne(i int) int { return i + 1 }
